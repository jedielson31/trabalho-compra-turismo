package com.jedi.comprapasseio.model;

/**
 * Created by jedi on 19/04/2017.
 *
 * View model para os itens da lista
 */

public class PontoTuristicoViewModel {

    private int id;
    private int imagem;
    private int titulo;
    private int descricao;
    private boolean vendido;

    public PontoTuristicoViewModel(int id, int imagem, int titulo, int descricao) {
        this.id = id;
        this.imagem = imagem;
        this.titulo = titulo;
        this.descricao = descricao;
        this.vendido = false;
    }

    public int getId() {
        return id;
    }

    public int getImagem() {
        return imagem;
    }

    public int getTitulo() {
        return titulo;
    }

    public int getDescricao() {
        return descricao;
    }

    public void setVendido(boolean vendido) {
        this.vendido = vendido;
    }

    public boolean getVendido() {
        return vendido;
    }
}
