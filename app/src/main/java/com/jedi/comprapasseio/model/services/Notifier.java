package com.jedi.comprapasseio.model.services;

/**
 * Created by jedi on 24/04/2017.
 *
 * Define um conjunto de métodos de notificação
 */

public interface Notifier {

    void sendNotification();

}
