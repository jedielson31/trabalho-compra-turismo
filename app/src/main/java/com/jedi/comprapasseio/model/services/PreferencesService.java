package com.jedi.comprapasseio.model.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.jedi.comprapasseio.R;
import com.jedi.comprapasseio.model.MetodoNotificacao;

/**
 * Created by jedi on 23/04/2017.
 *
 * Gerencia as preferencias do Usuário
 */

public class PreferencesService {

    private SharedPreferences sharedPreferences;
    private Context context;

    public PreferencesService(SharedPreferences sharedPreferences, Context context) {
        this.sharedPreferences = sharedPreferences;
        this.context = context;
    }

    public boolean possuiPreferenciasNecessarias() {
        return this.possuiNome();
    }

    public String getNome(){
        return this.sharedPreferences
                .getString(this.context.getString(R.string.preference_edit_text_nome_key), "");
    }

    public int getQuantidadeDeAcompanhantes(){
        return Integer.parseInt(this.sharedPreferences
                .getString(this.context.getString(R.string.preference_edit_text_quantidade_pessoas_key), "0"));
    }

    private boolean possuiNome(){
        return !this.getNome().equals("");
    }

    public boolean preencheuTodas() {
        return this.possuiNome();
    }

    public boolean reservouAlmoco() {
        return this.sharedPreferences.getBoolean(
                this.context.getString(R.string.preference_switch_almoco_key), false);
    }

    public MetodoNotificacao getMetodoNotificacao(){
        String defaultValue = this.context.getResources().getStringArray(R.array.opcoes_notificacao)[0];
        String value = this.sharedPreferences.getString(
                this.context.getString(R.string.preference_listchoice_metodo_notificacao_key), defaultValue);

        if(value.toUpperCase().equalsIgnoreCase("EMAIL")){
            return MetodoNotificacao.EMAIL;
        }

        if(value.toUpperCase().equalsIgnoreCase("SMS")){
            return MetodoNotificacao.SMS;
        }

        if(value.toUpperCase().equalsIgnoreCase("WHATSAPP")){
            return MetodoNotificacao.WHATSAPP;
        }

        return MetodoNotificacao.EMAIL;
    }
}
