package com.jedi.comprapasseio.model;

/**
 * Created by jedi on 23/04/2017.
 *
 * Metodos de notificação da compra
 */

public enum MetodoNotificacao {

    SMS("SMS"),
    EMAIL("EMAIL"),
    WHATSAPP("WHATSAPP");

    private String metodo;

    MetodoNotificacao(String value){
        this.metodo = value;
    }

    @Override
    public String toString() {
        return this.metodo;
    }
}
