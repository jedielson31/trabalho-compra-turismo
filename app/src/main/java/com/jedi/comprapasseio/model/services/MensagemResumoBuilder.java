package com.jedi.comprapasseio.model.services;

import android.content.Context;

import com.jedi.comprapasseio.R;

import java.util.List;

/**
 * Created by jedi on 24/04/2017.
 *
 * Ajuda a montar a mensagem de resumo
 */

public class MensagemResumoBuilder {

    private String stringBuilder;

    private Context context;

    private String nome;
    private List<String> parques;
    private int quantidadeAcompanhantes;
    private boolean incluirAlmoco;
    private int valor;

    public MensagemResumoBuilder(Context context) {
        this.context = context;
        this.stringBuilder = this.context.getString(R.string.activity_passeios_mensagem_resumo);
    }

    public MensagemResumoBuilder comNome(String nome) {
        this.nome = nome;

        if(nomeInvalido()){
            throw new IllegalArgumentException("nome");
        }

        return this;
    }

    public MensagemResumoBuilder comOsParques(List<String> parques) {
        this.parques = parques;
        if(parquesInvalido()){
            throw new IllegalArgumentException("parques");
        }
        return this;
    }

    public MensagemResumoBuilder comAQuantidadeDeAcompanhantes(int quantidadeAcompanhantes) {
        this.quantidadeAcompanhantes = quantidadeAcompanhantes;

        if(quantidadeAcompanhantesInvalida()){
            throw new IllegalArgumentException("quantidadeAcompanhantesInvalida");
        }

        return this;
    }

    public MensagemResumoBuilder incluindoAlmoco(boolean incluir) {
        this.incluirAlmoco = incluir;
        return this;
    }

    public MensagemResumoBuilder comOValor(int valor) {
        this.valor = valor;
        return this;
    }

    public String build() {

        if (nomeInvalido()) {
            throw new IllegalArgumentException("O nome é obrigatório. Por favor, chame o método \"comNome\"");
        }

        if (parquesInvalido()) {
            throw new IllegalArgumentException("Os parques são obrigatórios. Por favor, chame o método \"comOsParques\"");
        }

        if (quantidadeAcompanhantesInvalida()) {
            throw new IllegalArgumentException("A quantidade de acompanhantes não pode ser menor que zero. Por favor, chame o método \"comAQuantidadeDeAcompanhantes\"");
        }


        this.stringBuilder = this.stringBuilder.replace("$nome$", this.nome);
        this.stringBuilder = this.stringBuilder.replace("$listaDeParques$", this.getMensagemListaParques());
        this.stringBuilder = this.stringBuilder.replace("$mensagem_acompanhante$", this.getMensagemAcompanhante());
        this.stringBuilder = this.stringBuilder.replace("$mensagem_almoco$", this.getMensagemAlmoco());
        this.stringBuilder = this.stringBuilder.replace("$valor_passeio$", this.getValorPasseio());

        return this.stringBuilder;
    }

    private String getValorPasseio() {
        String mensagem = "$valor$,00";
        return mensagem.replace("$valor$", String.valueOf(this.valor));
    }

    private String getMensagemAlmoco() {
        String mensagem = this.context.getString(R.string.activity_passeios_mensagem_resumo_almoco);
        mensagem = this.incluirAlmoco
                ? mensagem.replace("$possui_almoco$", "")
                : mensagem.replace("$possui_almoco$", "não");

        return mensagem;
    }

    private String getMensagemAcompanhante() {

        if(this.quantidadeAcompanhantes == 0){
            return this.context.getString(R.string.activity_passeios_mensagem_resumo_acompanhante_nenhum);
        }

        if(this.quantidadeAcompanhantes == 1){
            return this.context.getString(R.string.activity_passeios_mensagem_resumo_acompanhante_um);
        }

        return this.context.getString(R.string.activity_passeios_mensagem_resumo_acompanhante_varios).replace("$quantidade$", String.valueOf(this.quantidadeAcompanhantes));
    }

    private String getMensagemListaParques() {

        if(this.parques.size() == 1){
            return this.parques.get(0);
        }

        String parquesMensagem = "";
        int total = this.parques.size() - 2;
        int tamanho = this.parques.size();
        for(int i = 0; i < total; i++){
            parquesMensagem += this.parques.get(i) + ", ";
        }

        parquesMensagem += this.parques.get(tamanho - 2) + " e " + this.parques.get(tamanho -1);
        return parquesMensagem;
    }

    private boolean nomeInvalido(){
        return this.nome == null || this.nome.equalsIgnoreCase("");
    }

    private boolean parquesInvalido() {

        return this.parques == null || this.parques.size() == 0;
    }

    private boolean quantidadeAcompanhantesInvalida() {
        return this.quantidadeAcompanhantes < 0;
    }
}
