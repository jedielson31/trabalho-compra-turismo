package com.jedi.comprapasseio.activities.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jedi.comprapasseio.model.services.Notifier;

/**
 * Created by jedi on 24/04/2017.
 * <p>
 * Envia uma mensagem por whatsapp
 */

public class WhatsAppActivity extends BaseNotifierActivity implements Notifier {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void sendNotification() {
        super.sendNotification();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getMessage());
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
        finish();
    }
}
