package com.jedi.comprapasseio.activities.passeios;

/**
 * Created by jedi on 23/04/2017.
 *
 * Listener para quando passeio é selecionado
 */

interface OnPasseioSelecionado {
    void atualizarValorTotal();
}
