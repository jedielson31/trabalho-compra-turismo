package com.jedi.comprapasseio.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.jedi.comprapasseio.R;
import com.jedi.comprapasseio.activities.passeios.PasseiosActivity;

public class MainActivity extends AppCompatActivity implements Runnable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Handler h = new Handler();
        h.postDelayed(this, 2000);
    }

    @Override
    public void run() {
        startActivity(new Intent(this, PasseiosActivity.class));
        finish();
    }
}
