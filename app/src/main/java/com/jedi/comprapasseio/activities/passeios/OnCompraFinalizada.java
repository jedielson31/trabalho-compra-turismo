package com.jedi.comprapasseio.activities.passeios;

/**
 * Created by jedi on 24/04/2017.
 *
 * Interface para listener de quando a compra está finalizada
 */

interface OnCompraFinalizada {

    void notificarCompra();
}
