package com.jedi.comprapasseio.activities.configuracoes;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;

import com.jedi.comprapasseio.R;

/**
 * Created by jedi on 23/04/2017.
 *
 * Cria um conjunto de preferências do usuário
 */

public class ConfiguracoesActivity extends PreferenceActivity {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }


}
