package com.jedi.comprapasseio.activities.passeios;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.jedi.comprapasseio.R;
import com.jedi.comprapasseio.model.PontoTuristicoViewModel;

import java.util.List;

/**
 * Created by jedi on 19/04/2017.
 * <p>
 * Adapter para a tela de seleção de itens
 */

class CustomItemsAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    private final List<PontoTuristicoViewModel> pontos;
    private final Activity activity;
    private final OnPasseioSelecionado listener;

    CustomItemsAdapter(List<PontoTuristicoViewModel> pontos, Activity activity, OnPasseioSelecionado listener) {
        this.pontos = pontos;
        this.activity = activity;
        this.listener = listener;
    }


    @Override
    public int getCount() {
        return this.pontos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.pontos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            convertView = InflateView(parent, holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        fillHolder(position, holder);

        return convertView;
    }

    private void fillHolder(int position, ViewHolder holder) {
        PontoTuristicoViewModel model = (PontoTuristicoViewModel) getItem(position);
        holder.imageViewPontoImagem.setImageResource(model.getImagem());
        holder.textViewTitulo.setText(model.getTitulo());
        holder.textViewDescricao.setText(model.getDescricao());
        holder.checkBoxSelected.setChecked(model.getVendido());
    }

    private View InflateView(ViewGroup parent, ViewHolder holder) {

        View view = this.activity.getLayoutInflater().inflate(R.layout.activity_passeios_list_item, parent, false);

        holder.imageViewPontoImagem = (ImageView) view.findViewById(R.id.image_view_ponto_imagem);
        holder.textViewTitulo = (TextView) view.findViewById(R.id.text_view_titulo);
        holder.textViewDescricao = (TextView) view.findViewById(R.id.text_view_descricao);
        holder.checkBoxSelected = (CheckBox) view.findViewById(R.id.check_box_selected);
        view.setTag(holder);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ViewHolder h = (ViewHolder) view.getTag();
        this.fillHolder(position, h);
        PontoTuristicoViewModel model = this.pontos.get(position);
        boolean vendido = !model.getVendido();
        model.setVendido(vendido);
        h.checkBoxSelected.setChecked(vendido);

        if(listener != null){
            listener.atualizarValorTotal();
        }
    }

    private static class ViewHolder {
        private ImageView imageViewPontoImagem;
        private TextView textViewTitulo;
        private TextView textViewDescricao;
        private CheckBox checkBoxSelected;
    }
}
