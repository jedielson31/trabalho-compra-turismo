package com.jedi.comprapasseio.activities.common;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.widget.Toast;

/**
 * Created by jedi on 24/04/2017.
 * <p>
 * Gerencia o envio de mensagens SMS
 */

public class SmsActivity extends BaseNotifierActivity {

    private final int PERMISSION_TO_SEND_SMS_REQUEST = 354645154;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void sendSms() {

        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage("+5541988798677", null, this.getMessage(), null, null);
        Toast.makeText(this, "Notificação Realizada por SMS.", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void sendNotification() {
        super.sendNotification();

        if (this.getMessage().equalsIgnoreCase("")) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
            sendSms();
            return;
        }

        boolean userGrantPermission = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS);

        if (!userGrantPermission) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_TO_SEND_SMS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode != this.PERMISSION_TO_SEND_SMS_REQUEST) {
            return;
        }

        if (grantResults.length == 0 || grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(this, "Permissão não concedida para envio de SMS", Toast.LENGTH_SHORT).show();
            return;
        }

        sendSms();
    }
}
