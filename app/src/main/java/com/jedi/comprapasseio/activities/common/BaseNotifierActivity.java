package com.jedi.comprapasseio.activities.common;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jedi.comprapasseio.model.services.Notifier;

/**
 * Created by jedi on 24/04/2017.
 *
 * Activity base para envio de notificações
 */

public class BaseNotifierActivity extends Activity implements Notifier {

    public static final String NOTIFICATION_MESSAGE = "SmsService_extra_message";

    private String message;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getIntent().getExtras();

        if(b == null){
            throw new NullPointerException("É obrigatório passar a mensagem no bundle da activity para envio de notificação");
        }

        this.message = b.getString(NOTIFICATION_MESSAGE, "");

        if(this.message.equalsIgnoreCase("")){
            throw new NullPointerException("É obrigatório passar a mensagem no bundle da activity para envio de notificação");
        }

        sendNotification();
    }


    String getMessage() {
        return message;
    }

    @Override
    public void sendNotification() {

    }
}
