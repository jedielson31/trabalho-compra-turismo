package com.jedi.comprapasseio.activities.passeios;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.jedi.comprapasseio.R;

/**
 * Created by jedi on 24/04/2017.
 *
 * Manipula a criação de uma caixa de dialogo para confirmação de compra
 */

class ConfirmacaoDialog {

    private final Context context;
    private final OnCompraFinalizada listener;
    private AlertDialog.Builder builder;

    ConfirmacaoDialog(Context context, OnCompraFinalizada listener){
        this.context = context;
        this.listener = listener;
    }

    void create(){
        builder = new AlertDialog.Builder(this.context);
        builder.setTitle(R.string.activity_passeios_confirmacao_dialog_title);
        builder.setPositiveButton(R.string.activity_passeios_confirmacao_dialog_positive_button_text, OkClick);
        builder.setNegativeButton(R.string.activity_passeios_confirmacao_dialog_negative_button_text, CancelClick);
    }

    boolean isCreated(){
        return this.builder != null;
    }

    void show(String mensagem){
        builder.setMessage(mensagem);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private final DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            if(listener != null){
                listener.notificarCompra();
            }

        }
    };

    private final DialogInterface.OnClickListener CancelClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    };
}
