package com.jedi.comprapasseio.activities.common;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jedi.comprapasseio.model.services.Notifier;

/**
 * Created by jedi on 24/04/2017.
 *
 * Gerencia o envio de mensagens via E-mail
 */

public class MailActivity extends BaseNotifierActivity implements Notifier {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void sendNotification() {
        super.sendNotification();

        String[] TO = {"jedielson.jn@gmail.com"};
        Intent it = new Intent(Intent.ACTION_SEND);

        it.setData(Uri.parse("mailto:"));
        it.setType("text/plain");
        it.putExtra(Intent.EXTRA_EMAIL, TO);
        it.putExtra(Intent.EXTRA_SUBJECT, "Confirmação de compra");
        it.putExtra(Intent.EXTRA_TEXT, this.getMessage());

        startActivity(Intent.createChooser(it, "Send Mail..."));
        finish();
    }
}
