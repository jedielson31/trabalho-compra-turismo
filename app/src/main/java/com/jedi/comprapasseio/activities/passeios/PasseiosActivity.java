package com.jedi.comprapasseio.activities.passeios;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jedi.comprapasseio.R;
import com.jedi.comprapasseio.activities.common.BaseNotifierActivity;
import com.jedi.comprapasseio.activities.common.MailActivity;
import com.jedi.comprapasseio.activities.common.SmsActivity;
import com.jedi.comprapasseio.activities.common.WhatsAppActivity;
import com.jedi.comprapasseio.activities.configuracoes.ConfiguracoesActivity;
import com.jedi.comprapasseio.model.MetodoNotificacao;
import com.jedi.comprapasseio.model.PontoTuristicoViewModel;
import com.jedi.comprapasseio.model.services.MensagemResumoBuilder;
import com.jedi.comprapasseio.model.services.PreferencesService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressWarnings("FieldCanBeLocal")
public class PasseiosActivity extends AppCompatActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener,
        OnPasseioSelecionado,
        OnCompraFinalizada {

    private static final String SELECIONADOS = "PasseiosActivity_Selecionados";
    @BindView(R.id.button_configuracoes)
    protected Button buttonConfiguracoes;

    @BindView(R.id.button_comprar)
    protected Button buttonComprar;

    @BindView(R.id.text_view_saudacao)
    protected TextView textViewSaudacao;

    @BindView(R.id.text_view_valor_total)
    protected TextView textViewValorTotal;

    protected SharedPreferences sharedPreferences;
    protected PreferencesService preferencesService;

    private List<PontoTuristicoViewModel> pontosTuristicos;
    private ListView listView;
    private CustomItemsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passeios);
        this.carregarPontos();

        this.prepararSelecionados(savedInstanceState);

        ButterKnife.bind(this);

        this.adapter = new CustomItemsAdapter(this.pontosTuristicos, this, this);
        this.listView = (ListView) findViewById(R.id.list_view_pontos_turisticos);
        this.listView.setOnItemClickListener(adapter);
        listView.setAdapter(adapter);

        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        this.preferencesService = new PreferencesService(this.sharedPreferences, this);
        this.atualizarTela();
    }

    private void prepararSelecionados(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            return;
        }

        ArrayList<String> selecionados = savedInstanceState.getStringArrayList(SELECIONADOS);
        if(selecionados == null){
            return;
        }

        for (String s: selecionados){
            for (PontoTuristicoViewModel m : this.pontosTuristicos){
                if( getString(m.getTitulo()).equalsIgnoreCase(s)){
                    m.setVendido(true);
                }
            }
        }
    }

    private void atualizarTela() {
        this.atualizarButtons();
        this.atualizarTextViews();
    }

    @SuppressWarnings("UnusedParameters")
    @OnClick(R.id.button_configuracoes)
    protected void buttonConfiguracoesClick(View v) {
        Intent i = new Intent(this, ConfiguracoesActivity.class);
        startActivityForResult(i, 1);
    }

    @SuppressWarnings("UnusedParameters")
    @OnClick(R.id.button_comprar)
    protected void buttonComprarClick(View v) {
        ConfirmacaoDialog dialog = new ConfirmacaoDialog(this, this);
        if (!dialog.isCreated()) {
            dialog.create();
        }

        dialog.show(this.montarResumo());
    }

    private void atualizarTextViewValorTotal() {
        int valor = calcularValor();
        String valorFormatado = getString(R.string.activity_passeios_text_view_valor_total_text)
                .replace("@valor@", String.valueOf(valor) + ",00");
        this.textViewValorTotal.setText(valorFormatado);
    }

    private void atualizarTextViews() {
        this.textViewSaudacao.setText(getTextViewSaudacaoText());
        atualizarTextViewValorTotal();
    }

    private void atualizarButtons() {

        if (preferencesService.preencheuTodas() && selecionouPasseio()) {
            this.buttonComprar.setEnabled(true);
            return;
        }

        this.buttonComprar.setEnabled(false);
    }

    private String getTextViewSaudacaoText() {

        if (!this.preferencesService.possuiPreferenciasNecessarias()) {
            return getString(R.string.activity_passeios_text_view_saudacao_text_usuario_nao_registrado);
        }

        String nome = this.preferencesService.getNome();
        return getString(R.string.activity_passeios_text_view_saudacao_text_usuario_registrado).replace("@nome@", nome);
    }

    private int calcularValor() {
        int quantidadePasseios = getQuantidadeDePasseios();
        int valor = 0;

        if (quantidadePasseios == 0) {
            return valor;
        }

        int quantidadePessoas = this.preferencesService.getQuantidadeDeAcompanhantes() + 1;
        valor = (quantidadePasseios * quantidadePessoas) * getResources().getInteger(R.integer.valor_passeio);

        if (this.preferencesService.reservouAlmoco()) {
            valor += quantidadePessoas * getResources().getInteger(R.integer.valor_almoco);
        }

        return valor;
    }

    private int getQuantidadeDePasseios() {
        return getPasseiosSelecionados().size();
    }

    private boolean selecionouPasseio() {
        return getQuantidadeDePasseios() > 0;
    }

    private void carregarPontos() {
        this.pontosTuristicos = new ArrayList<>();

        pontosTuristicos.add(new PontoTuristicoViewModel(1, R.drawable.catedral, R.string.Catedral, R.string.CatedralDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(2, R.drawable.jardim_botanico, R.string.JardimBotanico, R.string.JardimBotanicoDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(3, R.drawable.largo_da_ordem, R.string.LargoDaOrdem, R.string.LargoDaOrdemDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(4, R.drawable.memorial_ucraniano, R.string.MemorialUcraniano, R.string.MemorialUcranianoDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(5, R.drawable.museu_olho, R.string.MuseuOlho, R.string.MuseuOlhoDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(6, R.drawable.opera_arame, R.string.OperaArame, R.string.OperaArameDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(7, R.drawable.palacio_avenida, R.string.PalacioAvenida, R.string.PalacioAvenidaDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(8, R.drawable.rua_24_horas, R.string.Rua24Horas, R.string.Rua24HorasDescricao));
        pontosTuristicos.add(new PontoTuristicoViewModel(9, R.drawable.rua_das_flores, R.string.RuaDasFlores, R.string.RuaDasFloresDescricao));

    }

    private String montarResumo() {
        return new MensagemResumoBuilder(this)
                .comNome(this.preferencesService.getNome())
                .comOsParques(this.getPasseiosSelecionados())
                .comAQuantidadeDeAcompanhantes(this.preferencesService.getQuantidadeDeAcompanhantes())
                .incluindoAlmoco(this.preferencesService.reservouAlmoco())
                .comOValor(this.calcularValor())
                .build();
    }

    private List<String> getPasseiosSelecionados() {

        List<String> selecionados = new ArrayList<>();
        for (PontoTuristicoViewModel m : this.pontosTuristicos) {
            if (m.getVendido()) {
                selecionados.add(getString(m.getTitulo()));
            }
        }

        return selecionados;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        this.atualizarTela();
    }

    @Override
    public void atualizarValorTotal() {
        this.atualizarTextViewValorTotal();
        this.atualizarButtons();
    }

    @Override
    public void notificarCompra() {
        MetodoNotificacao notificacao = preferencesService.getMetodoNotificacao();

        switch (notificacao) {

            case SMS:
                this.notificarPorSMS();
                break;
            case EMAIL:
                this.notificarPorEmail();
                break;
            case WHATSAPP:
                this.notificarPorWhasApp();
                break;
        }
    }

    private void notificarPorEmail() {
        Intent it = getNotificationIntent(MailActivity.class);
        startActivity(it);
    }

    private void notificarPorSMS() {
        Intent it = getNotificationIntent(SmsActivity.class);
        startActivity(it);
    }

    private void notificarPorWhasApp() {
        Intent it = getNotificationIntent(WhatsAppActivity.class);
        startActivity(it);
    }

    private Intent getNotificationIntent(Class classe) {
        Intent it = new Intent(this, classe);
        it.putExtra(BaseNotifierActivity.NOTIFICATION_MESSAGE, this.montarResumo());
        return it;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<String> data = (ArrayList<String>) getPasseiosSelecionados();
        outState.putStringArrayList(SELECIONADOS, data);
    }
}